const AotPlugin = require("@ngtools/webpack").AngularCompilerPlugin;
const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");
const ContainerReferencePlugin = require("webpack/lib/container/ContainerReferencePlugin");
const ContainerPlugin = require("webpack/lib/container/ContainerPlugin");

const oceanConfig = {
  entry: ["./src/polyfills.ts", "./src/main.ts"],
  resolve: {
    mainFields: ["browser", "module", "main"]
  },
  devServer: {
    contentBase: path.join(__dirname, "dist/ocean"),
    port: 5000
  },
  module: {
    rules: [
      { test: /\.ts$/, loader: "@ngtools/webpack" }
    ]
  },
  plugins: [
    // ContainerReferencePlugin for Host allows to statically import shared libs
    new ContainerReferencePlugin({
      remoteType: 'var',
      remotes: {
        squirrel: "squirrel"
      },
      overrides: ["@angular/core", "@angular/common", "@angular/router"]
    }),
    new AotPlugin({
      skipCodeGeneration: false,
      tsConfigPath: "./tsconfig.app.json",
      directTemplateLoading: true,
      entryModule: path.resolve(
        __dirname,
        "./src/app/app.module#AppModule"
      )
    }),
    new HtmlWebpackPlugin({
      template: "./src/index.html"
    })
  ],
  output: {
    filename: "[id].[name].js",
    path: __dirname + "/dist/ocean",
    chunkFilename: "[id].[chunkhash].js"
  },
  mode: "production"
};

module.exports = oceanConfig;